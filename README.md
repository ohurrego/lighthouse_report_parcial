El script de Jenkins es el siguiente:
```javascript
node{
       def nodeHome = tool(name: 'nodejs-8.9.3')
        def buildEnv = [
             "PATH+NPM=${nodeHome}/bin"
       ]
       
       stage('Remove old builds'){
           properties([[$class: 'BuildDiscarderProperty', strategy: [$class: 'LogRotator', artifactDaysToKeepStr: '5', artifactNumToKeepStr: '', daysToKeepStr: '5', numToKeepStr: '10']]]);
       }
       
   stage('Prepare code') {
           withEnv (buildEnv) {
           sh 'git init'
           sh 'git config --local ""'
           git url: 'https://gitlab.com/ohurrego/lighthouse_report_parcial.git', branch: 'master', credetianlsId : 'gitlab2'
        }
    }

   stage('Installing dependencies'){
       withEnv (buildEnv) {
        sh 'npm config set registry http://registry.npmjs.org/'
        sh 'npm install'
       }
   }
   
   stage('Running audit'){
       withEnv (buildEnv) {
       sh 'app_url=https://losestudiantes.co/ npm run lighthouse:ci'
       }
   }
   
   stage('Publishing report'){
       publishReport();
   }
       
}

def publishReport(){
   publishHTML(target: [
   allowMissing: false,
   alwaysLinkToLastBuild: false,
   keepAll: true,
   reportDir: '.',
   reportFiles: 'lighthouse-report.html',
   reportName: "Lighthouse-LosProfesores-parcial"
   ])
}
```